from PIL import Image, ImageDraw, ImageFont
# create Image object with the input image

from datetime import datetime, timedelta

import glob

 
font = ImageFont.truetype('./fonts/Tanha.ttf', size=45)

 
imdir = './input_images/'
ext = ['png', 'jpg', 'gif']    # Add image formats here

files = []
[files.extend(glob.glob(imdir + '*.' + e)) for e in ext]
i=0
time = datetime.now()
for img in files:
    print(str(img))
    image = Image.open(str(img))

    draw = ImageDraw.Draw(image)

    time += timedelta(minutes=1)
    (x, y) = (250, 500)   #locations of your text
    name = time.strftime("%H:%M")
    color = 'rgb(255, 255, 255)' # white color
    draw.text((x, y), name, fill=color, font=font)
    file_name = "ovi"+str(i)
    i+=1
    image.save(('./export_images/{}.png').format(file_name))
    